from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


class EmailBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=kwargs.get('email'))
        except UserModel.DoesNotExist:
            raise ValueError("User does not exist")
        else:
            if user.check_password(password):
                return user
            else:
                raise ValueError("Incorrect password")

