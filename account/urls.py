from django.conf.urls import url
from django.urls import path
from account.views import signup, log_in

urlpatterns = [
    path('signup/', signup, name='signup'),
    path('login/', log_in, name='log_in'),
]

