from django.contrib.auth import login, authenticate, get_user_model, models
from django.db.utils import IntegrityError
from django.shortcuts import render, redirect
from django.http import HttpResponse
from account.forms import SignupForm, AuthenticationForm
from account.models import Address


User = get_user_model()


def home(request):
    if request.user.is_authenticated:
        context = {
            "user_name": request.user.username
        }
    else:
        context = {
            "user_name": "Guest"
        }
    return render(request, 'home.html', {'form': context})


def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            address = Address.objects.create(
                street_no=form_data["address_line_1"],
                address_line_1=form_data["address_line_2"],
                address_line_2=form_data["address_line_3"],
                state=form_data["state"],
                city=form_data["city"],
                country=form_data["country"],
                zip=form_data["zip_code"]
            )
            try:
                user = User.objects.create(
                    first_name=form_data["first_name"],
                    last_name=form_data["last_name"],
                    email=form_data["email"],
                    address=address
                )
                user.set_password(form_data["password1"])
            except IntegrityError:
                raise ValueError("User already exist")
            login(request, user)
            return HttpResponse('You have been logged in successfully')
    else:
        form = SignupForm()
    return render(request, 'accounts/signup.html', {'form': form})


def log_in(request):
    if request.method == 'POST':
        form = AuthenticationForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            try:
                user = authenticate(email=data['email'], password=data['password'])
                login(request, user)
                HttpResponse("Logged in success fully")
            except Exception as e:
                HttpResponse(e)
    form = AuthenticationForm()
    return render(request, 'accounts/log_in.html', {'form': form})
