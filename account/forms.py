from django import forms
from django.contrib.auth import password_validation
from django.utils.translation import gettext_lazy as _


class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=254)
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )
    address_line_1 = forms.CharField(max_length=30, help_text="House No")
    address_line_2 = forms.CharField(max_length=30)
    address_line_3 = forms.CharField(max_length=30)
    city = forms.CharField(max_length=30)
    state = forms.CharField(max_length=30)
    country = forms.CharField(max_length=30)
    zip_code = forms.CharField(max_length=30)

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2


class AuthenticationForm(forms.Form):
    email = forms.EmailField(max_length=256, required=True)
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
    )
